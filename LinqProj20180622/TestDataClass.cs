﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProj20180622
{
    public class TestDataClass
    {

        public Context GetCollection()
        {
            return CollectionCreator.GetContext();
        }

    }





    public class ConsultantTemp
    {

        public int ConsultantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<CompetenceTemp> Compentences { get; set; }

    }

    public class CompetenceTemp
    {
        public int CompetenceId { get; set; }
        public string CompetenceName { get; set; }
        public int Experience { get; set; }
    }
    
    public class Consultant
    {
        public int ConsultantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Competence
    {
        public int CompentenceId { get; set; }
        public string CompetenceName { get; set; }
    }

    public class ConsultantCompetence
    {
        public int ConsultantId { get; set; }
        public int CompetenceId { get; set; }
        public int Experience { get; set; }

    }

    public class Context
    {
        public IEnumerable<Consultant> Consultants { get; set; }
        public IEnumerable<Competence> Competences { get; set; }
        public IEnumerable<ConsultantCompetence> ConsultantCompetences { get; set; }
    }

    public static class CollectionCreator
    {
        public static Context GetContext()
        {
            var consultants = new List<Consultant>
        {
            new Consultant { ConsultantId = 1, FirstName = "Micke", LastName = "Kleven"},
            new Consultant { ConsultantId = 2, FirstName = "Kenneth", LastName = "Andersson"},
            new Consultant { ConsultantId = 3, FirstName = "Michal", LastName = "Dzienis"}
        };

            var competences = new List<Competence>
        {
            new Competence {CompentenceId = 1, CompetenceName = "C#"},
            new Competence {CompentenceId = 2, CompetenceName = "Jquery"},
            new Competence {CompentenceId = 3, CompetenceName = "java"},
            new Competence {CompentenceId = 4, CompetenceName = "json"},
            new Competence {CompentenceId = 5, CompetenceName = "type Script"},
            new Competence {CompentenceId = 6, CompetenceName = "Entity Framework"},
            new Competence {CompentenceId = 7, CompetenceName = "MVC"},
        };
            
            var consultantCompetences = new List<ConsultantCompetence>
        {
            //Consultant 1
            new ConsultantCompetence {ConsultantId = 1, CompetenceId = 1, Experience = 4},
            new ConsultantCompetence {ConsultantId = 1, CompetenceId = 6, Experience = 1},
            new ConsultantCompetence {ConsultantId = 1, CompetenceId = 7, Experience = 2},

            //Consultant 2
            new ConsultantCompetence {ConsultantId = 2, CompetenceId = 1, Experience = 3},
            new ConsultantCompetence {ConsultantId = 2, CompetenceId = 2, Experience = 3},
            new ConsultantCompetence {ConsultantId = 2, CompetenceId = 5, Experience = 2},

            //Consultant 3
            new ConsultantCompetence {ConsultantId = 3, CompetenceId = 1, Experience = 3},
            new ConsultantCompetence {ConsultantId = 3, CompetenceId = 2, Experience = 4},
            new ConsultantCompetence {ConsultantId = 3, CompetenceId = 5, Experience = 2},



        };

            return new Context
            {
                Consultants = consultants,
                Competences = competences,
                ConsultantCompetences = consultantCompetences
            };
        }
    }
}
