﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProj20180622
{
    class Program
    {
        static Context context; 

        static void Main(string[] args)
        {
            TestDataClass testDataClass = new TestDataClass();

            context = testDataClass.GetCollection();
            List<ConsultantTemp> _consultants = new List<ConsultantTemp>();


            ConsultantTemp _consTemp;

            foreach (var co in context.Consultants)
            {
                _consTemp = new ConsultantTemp
                {
                    ConsultantId = co.ConsultantId,

                    Compentences = GetCompetences(co.ConsultantId),

                    FirstName = (from coID in context.Consultants
                                 where coID.ConsultantId == co.ConsultantId
                                 select coID.FirstName).SingleOrDefault(),

                    LastName = (from coID in context.Consultants
                                where coID.ConsultantId == co.ConsultantId
                                select coID.LastName).SingleOrDefault()
                };

                _consultants.Add(_consTemp);
            }
        }


        // Getting the collection with competence names and experiences based on id's 
        // from the ConsultantComepentence table. 
        static IEnumerable<CompetenceTemp> GetCompetences(int _consultantId)
        {

            List<CompetenceTemp> _competences = new List<CompetenceTemp>();

            // current linq query is 
            var query =
               from post in context.ConsultantCompetences
               join meta in context.Competences on post.CompetenceId equals meta.CompentenceId
               where post.ConsultantId == _consultantId
               select new { Meta = meta.CompetenceName, Post = post.Experience  };

            CompetenceTemp compTemp = new CompetenceTemp();

            foreach (var item in query)
            {
                compTemp = new CompetenceTemp
                {
                    CompetenceName = item.Meta,
                    Experience = item.Post
                }; 

                _competences.Add(compTemp); 
            }

            return _competences;

        }
        
    }
}
